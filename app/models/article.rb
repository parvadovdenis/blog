class Article < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  has_many :comms, through: :comments
end
