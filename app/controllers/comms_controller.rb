class CommsController < ApplicationController

  def create
    @comment = Comment.find(params[:comment_id])
    @comm = @comment.comms.build(comm_params)
    @comm.users_id = current_user.id
    @comm.commenter = current_user.email
    if @comm.save
      redirect_to comment_path(@comment)
    else
      redirect_to comment_path(@comment)
    end
  end

  def update
    @comm = Comm.find(params[:id])
    if @comm.users_id == current_user.id
      if @comm.update(comm_params)
        redirect_to comm_path(@comm), notice: 'Item was changed'
      else
        render 'edit'
      end
    else
      redirect_to comm_path(@comm), notice: 'You can`t edit or delete this'
    end
  end

  def edit
    @comm = Comm.find(params[:id])
  end

  def destroy
    @comment = Comment.find(params[:comment_id])
    @comm = @comment.comms.find(params[:id])
    if @comm.users_id == current_user.id
      @comm.destroy
      redirect_to comment_path(@comment), notice: 'Item was removed'
    else
      redirect_to comment_path(@comment), notice: 'You can`t edit or delete this'
    end
  end

  def show
    @comm = Comm.find(params[:id])
  end


  private
  def comm_params
    params.require(:comm).permit(:commenter, :body)
  end

end
