class ArticlesController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]

  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
  end

  def create

    @article = Article.new(article_params)

    @article.users_id = current_user.id
    @article.title = current_user.email

    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  def update
    @article = Article.find(params[:id])
    if @article.users_id == current_user.id
      if @article.update(article_params)
        redirect_to @article, notice: 'Item was changed'
      else
        render 'edit'
      end
    else
      redirect_to articles_path, notice: 'You can`t edit or delete this'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    if @article.users_id == current_user.id
      @article.destroy
      redirect_to articles_path, notice: 'Item was removed'
    else
      redirect_to articles_path, notice: 'You can`t edit or delete this'
    end
  end

  private
  def article_params
    params.require(:article).permit(:title, :text)
  end
end