class CommentsController < ApplicationController

  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.build(comment_params)
    @comment.users_id = current_user.id
    @comment.commenter = current_user.email
    if @comment.save
      redirect_to article_path(@article)
    else
      article_path(@article)
    end

  end

  def update
    @comment = Comment.find(params[:id])
    if @comment.users_id == current_user.id
      if @comment.update(comment_params)
        redirect_to comment_path(@comment), notice: 'Item was changed'
      else
        render 'edit'
      end
    else
      redirect_to comment_path(@comment), notice: 'You can`t edit or delete this'
    end
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    if @comment.users_id == current_user.id
      @comment.destroy
      redirect_to article_path(@article), notice: 'Item was removed'
    else
      redirect_to article_path(@article), notice: 'You can`t edit or delete this'
    end

  end

  def show
    @comment = Comment.find(params[:id])
  end

  private
  def comment_params
    params.require(:comment).permit(:body, :users_id)


  end


end
