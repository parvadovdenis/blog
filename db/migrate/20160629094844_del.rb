class Del < ActiveRecord::Migration
  def change
    drop_table :articles
    drop_table :comments
    drop_table :comms
    drop_table :users
  end
end
